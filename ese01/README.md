# esercizio 01

da dentro il vostro repositery locale, lanciate una git bash e lanciate i comandi :

- `git fetch`
- `git pull` 
- `cd ese01` 


dopo aver letto i capitoli da 
["HTML home"](https://www.w3schools.com/html/default.asp) a 
[" HTML Attributes"](https://www.w3schools.com/html/html_attributes.asp) 
del tutorial su w3school modificare il file index.html creando una pagina contenente due link :
- uno al sito della scuola
- un'altro al motore di ricerca google 

per pubblicare i file sul vostro repositery remoto dalla git bash (da dentro la directory ese01) lanciare i comandi:

- `git add index.html`
- `git commit -m " consegna ... o un messagio a vostra scelta"`
- `git push `



